Stephanie Emila
================

#### Requirments:

* [Node & NPM](http://nodejs.org/)
* [Bower](http://bower.io/)
* [Grunt](http://gruntjs.com/getting-started)
* [Sass](http://sass-lang.com/install)
* [LiveReload Chrome Plugin](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei)

## Getting Started

Install client side dependencies:

`bower install`

Install grunt and server side dependencies

`npm install`

Start the server locally

`grunt`

## Grunt Tasks

`grunt` : starts node server, watches for changes to backend and front-end code and triggers livereload

`grunt build` : optimizes resource files (js, css, images) for production environments
